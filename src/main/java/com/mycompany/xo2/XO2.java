/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.xo2;

/**
 *
 * @author informatics
 */
public class XO2 {

    public static void main(String[] args) {
        printWelcome();
        showTable();
    }

    private static void printWelcome() {
        System.out.println(x: "Welcome to OX Game");
    }

    private static void showTable() {
        for (int r = 0; r < 3; r++) {
            System.out.print(x: "- ");
            for (int c = 0; c < 3; c++) {
                System.out.print(x: "- ");
            }
            System.out.println();
        }
    }
}
